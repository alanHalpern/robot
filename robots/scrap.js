const rp = require('request-promise');
const $ = require('cheerio');
const date = require('date-and-time');

const state = require('./state.js')

var now = new Date();
 
const url = 'https://www.wsj.com/news/archive/' + date.format(now,'YYYYMMDD') ;


async function robot() {
  rp(url)
    .then(function(html){
    //success!
      // get all articles
      const articles = $('ol > article', html);
      const TotalOfArticles = articles.length;

      const news = [];
      var ArrayNumb = 0;

      for (let i = 0; i < TotalOfArticles; i++) {
        // find the type
        var type = articles[i].children[1].children[0].children[0].children[0].data ;

          if( type == ('Business')|| type ==('Tech')|| type ==('Pro PE Deals')|| type ==('Media & Marketing')|| type ==('Heard on the Street')){
            //find the rest
            var title = articles[i].children[1].children[0].children[1].children[0].children[0].data ;
            var resume = articles[i].children[1].children[1].children[0].children[0].data ;
            // create info array
            var SingleNewsInfo =   {'type': type , 'title': title, 'resume': resume }
            // push in news
            news[ArrayNumb] = SingleNewsInfo
            ArrayNumb++
           }
      }
      state.save(news)

    })
    .catch(function(err){
      //handle error
    });
    
}

module.exports = robot