# Credentials format

## Algorithmia

File: `algorithmia.json`

```
{
  "apiKey": "sim8As72h56j3mBYqEOSHfLE4k81"
}
```

## Watson Natural Language Understanding

File: `watson-nlu.json`

```
{
  "apikey": "7H_cOJGK39_h_5FA7CYJBodgaWqWGrScnNrGznGoJ-UM",
  "iam_apikey_description": "Auto generated apikey during resource-key operation for Instance - crn:v1:bluemix:public:natural-language-understanding:us-south:a/33d21f5f42a644ca9810769363c5e75b:04229d0f-263b-4618-a751-edac9947aff0::",
  "iam_apikey_name": "auto-generated-apikey-c6848034-dd8f-4e78-9a7c-c0025e6040a0",
  "iam_role_crn": "crn:v1:bluemix:public:iam::::serviceRole:Manager",
  "iam_serviceid_crn": "crn:v1:bluemix:public:iam-identity::a/33d21f5f42a644ca9810769363c5e75b::serviceid:ServiceId-57ce3e6f-cf37-49cb-8cfb-98049643bfa8",
  "url": "https://gateway.watsonplatform.net/natural-language-understanding/api" 
}
```
